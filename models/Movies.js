const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const movieSchema = new Schema({
    name: {type: String, required: true},
    category: {type: String, required: true},
    description: {type: String, required: true},
    age: {type: Number, required: true},
    casting: {type: String, required: true},
    duration: {type: Number, required: true},
    image: {type: String}
}, {
    timestamps: true,
})

const Movie = mongoose.model('Movie', movieSchema);

module.exports = Movie;