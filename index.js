const express= require('express');
const db = require('./db');
db.connect();

const PORT = 5000;

const app = express();

const router = express.Router();

router.get('/', (req, res, next) => {
    return res.send('streamly')
});

app.use('/', router);

//creamos el middleware encargado de capturar todas las rutas:

app.use('*', (req, res, next) => {
    const error = new Error('Route not found');
    error.status = 404;
    next(error);
});

//creamos el middleware para la gestion de errores

app.use((error, req, res, next) =>{
    return res.status(error.status || 500).json(error.message||'Unexpected error');
});

app.listen(PORT, () => {
    console.log(`Server running in http://localhost:${PORT}`)
});
